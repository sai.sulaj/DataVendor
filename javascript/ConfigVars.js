/**
 * Author: Saimir Sulaj.
 * Date: May 20, 2018.
 * Purpose: Holds constants and configuration variables.
 */

// ------------- DEFAULT VALS ------------- //

const OB_CACHE_TIME_RANGE = 120 * 60000;
const VERBOSE             = 2;
const UPDATE_PERIOD       = 2500;
const OB_ENTRY_DEPTH      = 3;

// Active exchanges.
EXCHANGE_TOGGLE = {
  POLONIEX: true,
  BITTREX: true,
  CEX: true
};

// Active symbols.
SYMBOL_TOGGLE = {
  DASH_USDT: true,
  XMR_USDT: true
}

// ------------- CLASS BLUEPRINT ------------- //

class ConfigVars {

  static get OB_CACHE_TIME_RANGE() {
    return OB_CACHE_TIME_RANGE;
  }

  static get VERBOSE() {
    return VERBOSE;
  }

  static get UPDATE_PERIOD() {
    return UPDATE_PERIOD;
  }

  static get OB_ENTRY_DEPTH() {
    return OB_ENTRY_DEPTH;
  }

  /**
   * Summary: Fetches exchange-specific data.
   * 
   * @param {JSON} _ExName JSON object of exchange data. 
   *                       Nullable.
   */
  static getExchangeData(_ExName) {
    _ExName = _ExName.toUpperCase();
    if (EXCHANGE_DATA.hasOwnProperty(_ExName)) {
      return EXCHANGE_DATA[_ExName];
    } else {
      return null;
    }
  }

  /**
   * Summary: Iterates EXCHANGE_TOGGLE object and calls callback
   *          function with exchange name and enabled status.
   * 
   * @since  0.0.1.
   * @access public.
   * 
   * @param {Function} _Callback Function with exchange name string
   *                             and boolean enabled status as params.
   *                             exchange name is all caps.
   */
  static async iterateExchangeToggles(_Callback) {
    for (var exName in EXCHANGE_TOGGLE) {
      if (EXCHANGE_TOGGLE.hasOwnProperty(exName)) {
        await _Callback(exName, EXCHANGE_TOGGLE[exName]);
      }
    }
  }

  /**
   * Summary: Iterates SYMBOL_TOGGLE object and calls callback
   *          function with symbol name and enabled status.
   * 
   * @since  0.0.1.
   * @access public.
   * 
   * @param {Function} _Callback Function with symbol name string
   *                             and boolean enabled status as params.
   *                             symbol name is all caps, and "/" is 
   *                             replaced with "_".
   */
  static iterateSymbolToggles(_Callback) {
    for (var symbName in SYMBOL_TOGGLE) {
      if (SYMBOL_TOGGLE.hasOwnProperty(symbName)) {
        _Callback(symbName, SYMBOL_TOGGLE[symbName]);
      }
    }
  }
}

module.exports = ConfigVars;