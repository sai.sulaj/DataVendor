/**
 * Author: Saimir Sulaj.
 * Date: May 20, 2018.
 * Purpose: Wrapper for CCXT exchange object.
 */

// -------------- DEPENDENCIES -------------- //

const MACD           = require('technicalindicators').MACD;

// -------------- LOCAL DEPENDENCIES -------------- //

const ConfigVars     = require('./ConfigVars.js');
const orderbookCache = require('./caches/OrderbookCache.js');

// -------------- CONSTANTS & GLOBAL VARS -------------- //

const PRICE_INDEX    = 0;
const VOL_INDEX      = 1;

// -------------- CLASS BLUEPRINT -------------- //

class ExchangeWrapper {

  /**
   * Summary: Initializes object.
   * 
   * @since: 0.0.1
   * 
   * @param {CCXT} _CCXT CCXT object to wrap.
   */
  constructor(_CCXT) {
    this.ccxt           = _CCXT;

    this.orderbookCache = new orderbookCache();
  }

  /**
   * Summary: Calculates MACD of cached orderbook data.
   * 
   * @param {String} _Symb Requested data symbol.
   * @param {JSON} _Options Options required to calculate MACD.
   * 
   * @since 0.0.1.
   * @access public.
   * 
   * @return {JSON} JSON of MACD, signal, and histogram.
   */
  getOBMACD(_Symb, _Options) {
    let oldestEntry = this.orderbookCache.getOld(_Symb, 1)[0];
    if (oldestEntry !== null) {
      let range = Date.now() - oldestEntry['timestamp'];
      let dataRaw = this.orderbookCache.queryByTimestamp(_Symb, range, _Options['period']);

      let dataParsed = dataRaw.map((entry) => {
        return entry[_Options['side']][0][PRICE_INDEX];
      });

      let macdInput = {
        values: dataParsed,
        fastPeriod: _Options['fast_period'],
        slowPeriod: _Options['slow_period'],
        signalPeriod: _Options['signal_period'],
        SimpleMAOscillator: false,
        SimpleMASignal: false
      }
      let macdOutput = MACD.calculate(macdInput);
      return macdOutput[macdOutput.length - 1];
    } else {
      return null;
    }
  }
}

module.exports = ExchangeWrapper;