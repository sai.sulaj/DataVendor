/**
 * Author: Saimir Sulaj.
 * Date: May 20, 2018.
 * Purpose: Cache for orderbook data.
 */

// -------------- LOCAL DEPENDENCIES -------------- //
 
const CustomUtils   = require('../CustomUtils.js');
const ConfigVars    = require('../ConfigVars.js');

// -------------- CLASS BLUEPRINT -------------- //

class OrderbookCache {

  constructor() {
    this.cache = {};
  }

  /**
   * Summary: Returns number of entries saved in cache.
   * 
   * @param {String} _Symb Symbol to query.
   */
  getNumEntries(_Symb) {
    _Symb = _Symb.toLowerCase();
    if (this.cache.hasOwnProperty(_Symb)) {
      return this.cache[_Symb].length;
    } else {
      return null;
    }
  }

  getRecent(_Symb, _Depth) {
    _Symb = _Symb.toLowerCase();

    if (!this.cache.hasOwnProperty(_Symb) ||
        this.cache[_Symb].length === 0) {
      return null;
    }

    if (_Depth !== null && _Depth !== undefined) {
      if (_Depth > this.cache[_Symb].length) {
        return null;
      } else {
        let length = this.cache[_Symb].length;
        return this.cache[_Symb].slice(length - _Depth, length);
      }
    } else {
      return this.cache[_Symb];
    }
  }

  getOld(_Symb, _Depth) {
    _Symb = _Symb.toLowerCase();

    if (!this.cache.hasOwnProperty(_Symb) ||
        this.cache[_Symb].length === 0) {
      return null;
    }

    if (_Depth !== null && _Depth !== undefined) {
      if (_Depth > this.cache[_Symb].length) {
        return null;
      } else {
        return this.cache[_Symb].slice(0, _Depth);
      }
    } else {
      return this.cache[_Symb];
    }
  }

  queryByTimestamp(_Symb, _Range, _Period) {
    _Period = parseInt(_Period);

    if (!this.cache.hasOwnProperty(_Symb) ||
        this.cache[_Symb].length === 0) {
      return null;
    }

    let now = Date.now();
    let oldestTimestamp = this.getOld(_Symb, 1)[0]['timestamp'];
    let entries = this.cache[_Symb];

    let range = (now - _Range) < oldestTimestamp ? now - oldestTimestamp : _Range;

    let tmpArray = [];

    for (var time = now - range; time <= now; time += _Period) {
      let entry = CustomUtils.nearestTimestamp(entries, time);
      tmpArray.push(entry);
    }

    return tmpArray;
  }

  cacheEntry(_Symb, _Entry) {
    // Handle absence of timestamp.
    if (!_Entry.hasOwnProperty('timestamp') ||
        _Entry['timestamp'] === null ||
        _Entry['timestamp'] === undefined) {
      _Entry['timestamp'] = Date.now();
    }

    if (this.cache.hasOwnProperty(_Symb)) {
      this.cache[_Symb].push(_Entry);

      let now = Date.now();
      let targetIndex = this.cache[_Symb].findIndex((entry) => {
        return entry['timestamp'] < now - (ConfigVars.OB_CACHE_TIME_RANGE);
      });
      if (targetIndex !== -1) {
        let length = this.cache[_Symb].length
        this.cache[_Symb] = this.cache[_Symb].slice(targetIndex, length - 1);
      }
    } else {
      this.cache[_Symb] = [_Entry];
    }

    console.log(`${_Symb} cache range: ${this.getCacheRange(_Symb)}`);
  }

  getCacheRange(_Symb) {
    if (this.cache.hasOwnProperty(_Symb)) {
      let now = Date.now();
      let oldestTimestamp = this.cache[_Symb][0]['timestamp'];
      let delta = now - oldestTimestamp;
      return CustomUtils.toReadableDuration(delta);
    } else {
      return null;
    }
  }
}

module.exports = OrderbookCache;