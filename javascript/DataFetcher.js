/**
 * Author: Saimir Sulaj.
 * Date: May 20, 2018.
 * Purpose: Fetches data from exchanges.
 */

// ------------- DEPENDENCIES ------------- //

const CCXT            = require("ccxt");

// ------------- LOCAL DEPENDENCIES ------------- //

const ConfigVars      = require("./ConfigVars.js");
const ExchangeWrapper = require('./ExchangeWrapper.js');

// ------------- CLASS BLUEPRINT ------------- //

class DataFetcher {

  constructor() {
    this.activeExchanges = [];
    this.activeSymbols   = [];
    this.symbolIndex     = 0;
    this.exchangeIndex   = 0;
    this.continueRunning = true;
  }

  /**
   * Summary: Gets status info of data.
   * 
   * @since 0.0.1.
   * @access public.
   * 
   * @return {JSON} JSON of heirarchy Exchanges -> Symbols -> Data.
   */
  getStatus() {
    let data = {};

    for (var ex = 0; ex < this.activeExchanges.length; ex++) {
      data[this.activeExchanges[ex].ccxt.name.toLowerCase()] = {};
      for (var symb = 0; symb < this.activeSymbols.length; symb++) {
        data[this.activeExchanges[ex].ccxt.name.toLowerCase()][this.activeSymbols[symb]] = {
          oldestEntry: this.activeExchanges[ex].orderbookCache.getOld(this.activeSymbols[symb], 1)[0]['timestamp']
        }
      }
    }

    return data;
  }

  /**
   * Summary: Fetches exchange stored in cache.
   * 
   * @since 0.0.1.
   * @access public.
   * 
   * @param {String} _Name Exchange name.
   * 
   * @return {ExchangeWrapper} Exchange object.
   */
  getExchange(_Name) {
    let index = this.activeExchanges.findIndex((ex) => {
      return ex.ccxt.name.toLowerCase() == _Name.toLowerCase();
    });
    if (index !== -1) {
      return this.activeExchanges[index];
    } else {
      return null;
    }
  }

  /**
   * Summary: Loads required exchange and symbol data.
   * 
   * @since 0.0.1
   * @access public.
   */
  async init() {
    console.log(`Initializing DataFetcher...`);
    this.activeSymbols = this.__LoadActiveSymbols();
    this.activeExchanges = await this.__LoadActiveExchanges();
    console.log(`DataFetcher initialized.`);
  }

  /**
   * Summary: Iterates through each activated exchange and symbol and 
   *          downloads data.
   * 
   * @since 0.0.1
   * @access public.
   * 
   * @param {method} _Callback Callback function on delay.
   */
  async runLoop(_Callback) {
    if (ConfigVars.VERBOSE >= 2) { console.log(`:DataFetcher#runLoop`); }

     while(this.continueRunning) {
        let iterationStartTime = Date.now();

        try {
          console.log(`ExI: ${this.exchangeIndex} | ExName: ${this.activeExchanges[this.exchangeIndex].ccxt.name} | SymbI: ${this.symbolIndex} | SymbName: ${this.activeSymbols[this.symbolIndex]}`);

          let ex = this.activeExchanges[this.exchangeIndex];
          let symb = this.activeSymbols[this.symbolIndex];

          if (ex.ccxt.symbols.includes(symb.toUpperCase())) {
            await ex.ccxt.fetchOrderBook(symb.toUpperCase(), ConfigVars.OB_ENTRY_DEPTH).then((OBEntry) => {
              ex.orderbookCache.cacheEntry(symb, OBEntry);
            }).catch((err) => {
              console.log(err.stack);
            });
          } else {
            console.log(`${ex.ccxt.name} does not support ${symb}, skipping...`);
          }

          let triggerDelay = this.__IncrementExAndSymbIndices();

          if (triggerDelay) {
            await _Callback();

            let delayVariation = 0;
            let iterationTime = Date.now() - iterationStartTime;
            if (iterationTime < ConfigVars.UPDATE_PERIOD) {
              delayVariation = -iterationTime;
            } else {
              delayVariation = -ConfigVars.UPDATE_PERIOD;
            }

            await new Promise(resolve => setTimeout(resolve, ConfigVars.UPDATE_PERIOD + delayVariation));
          }
          console.log('\n'.repeat(2));
        } catch(err) {
          console.log(err.stack);
          this.continueRunning = false;
        }
     }
  }

   /**
    * Symmary: Loads activated symbols from config file.
    * 
    * @since 0.0.1
    * @access private.
    * 
    * @return {Array<String} Array of symbol names in format: '{BASE}/{QUOTE}',
    *                        lowercase.
    */
  __LoadActiveSymbols() {
    if (ConfigVars.VERBOSE >= 2) { console.log(`:DataFetcher#__LoadActiveSymbols`); }
  
    let tmpArray = [];
  
    ConfigVars.iterateSymbolToggles((_SymbName, _Status) => {
      if (_Status) {
        tmpArray.push(_SymbName.replace('_', '/').toLowerCase());
      }
    });
  
    console.log(`Symbols loaded.`);
    return tmpArray;
  }

  /**
   * Summary: Retrieves active exchange data from config file, and loads markets.
   * 
   * @since 0.0.1
   * @access private.
   * 
   * @return {Array<CCXT>} Array of activated exchanges.
   */
  async __LoadActiveExchanges() {
    if (ConfigVars.VERBOSE >= 2) { console.log(`:DataFetcher#__LoadActiveExchanges`); }

    let tmpArray = [];
    let _this = this;

    await ConfigVars.iterateExchangeToggles(async (_ExName, _Status) => {
      if (_Status) {
        tmpArray = await _this.__ActivateExchange(tmpArray, _ExName.toLowerCase());
      }
    });

    return tmpArray;
  }

  /**
   * Summary: Validates and loads market for exchange. Automatically
   *          removes exchange if any errors found.
   * 
   * @param {Array<ExchangeWrapper>}   _Array Array to add exchange to.
   * 
   * @since 0.0.1
   * @access private.
   * 
   * @return {Array<ExchangeWrapper>}  Passed array with that may have
   *                                     specified exchange in it
   */
  async __ActivateExchange(_Array, _ExchangeName) {
    if (ConfigVars.VERBOSE >= 2) { console.log(`:DataFetcher#__ActivateExchange -> _Array (length): ${_Array.length}, _ExchangeName: ${_ExchangeName.replace('_', '.')}`); }
  
    let index = _Array.push(new ExchangeWrapper(new CCXT[_ExchangeName]())) - 1;
  
    try {
      await _Array[index].ccxt.loadMarkets();
    } catch(err) {
      console.log(`#activateExchange -> ERROR: COULD NOT INIT ${_Array[index].ccxt.name}:\n${err.stack}\nREMOVING FROM ACTIVE EXCHANGES...`);
      _Array.splice(index, 1);
    }
    return _Array;
  }

  /**
   * Summary: Increments exchange and symbol indices so that program iterates through each exchange per
   *          symbol.
   * 
   * @since 0.0.1
   * @access private.
   * 
   * @return {Boolean} True if symbols have been switched.
   */
  __IncrementExAndSymbIndices() {
    if (ConfigVars.VERBOSE >= 2) { console.log(`:DataFetcher#__IncrementExAndSymbIndices`); }
  
    if (this.exchangeIndex === this.activeExchanges.length - 1) {
      this.exchangeIndex = 0;
      if (this.symbolIndex === this.activeSymbols.length - 1) {
        this.symbolIndex = 0;
      } else {
        this.symbolIndex++;
      }
      return true;
    } else {
      this.exchangeIndex++;
      return false;
    }
  }
}

module.exports = DataFetcher;