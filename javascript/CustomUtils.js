/**
 * Author: Saimir Sulaj.
 * Date: May 21, 2018.
 * Purpose: Class that holds random useful custom utils.
 */

// -------------- CLASS BLUEPRINT -------------- //

class CustomUtils {

  /**
   * Summary: Uses Binary Search algorithm to find data entry with closest 'timestamp' child.
   * 
   * @since 0.0.1
   * @access public.
   * 
   * @param {Array[JSON]} _Data Array of data to query.
   * @param {int} _Timestamp Target timestamp.
   * 
   * @return {JSON} Entry in _Data with nearest timestamp child.
   */
  static nearestTimestamp(_Data, _Timestamp) {
    if (_Data[0].timestamp <= _Timestamp) {
      return _Data[0];
    }
    if (_Data[_Data.length - 1].timestamp >= _Timestamp) {
      return _Data[_Data.length - 1];
    }

    let i = 0,
        j = _Data.length,
        mid = 0;
    while (i < j) {
      mid = Math.floor((i + j) / 2);

      if (_Data[mid].timestamp === _Timestamp) {
        return _Data[mid];
      }

      if (_Timestamp > _Data[mid].timestamp) {
        if (mid > 0 && _Timestamp < _Data[mid - 1].timestamp) {
          if (_Data[mid - 1].timestamp - _Timestamp >= _Timestamp - _Data[mid].timestamp) {
            return _Data[mid];
          } else {
            return _Data[mid - 1];
          }
        }

        j = mid;
      } else {
        if (mid < _Data.length && _Timestamp > _Data[mid + 1].timestamp) {
          if (_Data[mid].timestamp - _Timestamp >= _Timestamp - _Data[mid + 1].timestamp) {
            return _Data[mid + 1];
          } else {
            return _Data[mid];
          }
        }
        i = mid + 1;
      }
    }
    return _Data[mid];
  }

  /**
   * Summary: Converts length of time to milliseconds.
   * 
   * @since 0.0.1
   * @access public.
   * 
   * @param {int} _Days    Number of days.
   * @param {int} _Hours   Number of hours.
   * @param {int} _Minutes Number of minutes.
   * @param {int} _Seconds Number of seconds.
   * 
   * @return {int}         Number of milliseconds represented by parameters.
   */
  static toMilliseconds (_Days, _Hours, _Minutes, _Seconds) {
    if (_Days === undefined || _Hours === undefined || _Minutes === undefined || _Seconds === undefined) {
      throw `ERROR: Invalid parameter -> A parameter is undefined: _Days: ${_Days}, _Hours: ${_Hours}, _Minutes: ${_Minutes}, _Seconds: ${_Seconds}`;
    }

    let conversionConstants = [86400000, 3600000, 60000, 1000];
    let paramsArray = [_Days, _Hours, _Minutes, _Seconds];
    let sum = 0;
    for (var i = 0; i < conversionConstants.length; i++) {
      sum += conversionConstants[i] * paramsArray[i];
    }
    return sum;
  }

  /**
   * Summary: Converts milliseconds to human readable format.
   * 
   * @since 0.0.1
   * @access public.
   * 
   * @param {int} _Milliseconds Milliseconds to convert.
   * 
   * @returns {String}          Output string in the form "{DAYS} days,
   *                            {HOURS} hours, {MINUTES} minutes, {SECONDS} seconds".
   */
  static toReadableDuration (_Milliseconds) {
    
    let conversionConstants = [86400000, 3600000, 60000, 1000];
    let outputArray = [0, 0, 0, 0];
    let outputUnits = ['days', 'hours', 'mins', 'secs'];
    
    for (var i = 0; i < conversionConstants.length; i++) {
      let coefficient = Math.floor(_Milliseconds / conversionConstants[i]);
      outputArray[i] = coefficient;
      _Milliseconds -= coefficient * conversionConstants[i];
    }
    
    let outputStr = '';
    for (var i = 0; i < outputArray.length; i++) {
      if (outputArray[i] === 0) { continue; }
      outputStr += `${outputArray[i]} ${outputUnits[i]}`;
      if (i < outputArray.length - 1) { outputStr += ', '; }
    }
    
    return outputStr;
  }
}

module.exports = CustomUtils;