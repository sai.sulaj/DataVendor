/**
 * Author: Saimir Sulaj.
 * Date: May 20, 2018.
 * Purpose: Handles all inbound network requests.
 */

// ------------- DEPENDENCIES ------------- //

const Express    = require("express");
const BodyParser = require("body-parser");

// ------------- CLASS BLUEPRINT ------------- //

class RequestHandler {

  constructor() {
    this.app = Express();
  }

  __ConfigExpress() {
    this.app.use(BodyParser.urlencoded({ extended: false }));
  }

  __InitRoutes() {
    var _this = this;
    this.app.get('/api/getMACD', (req, res) => {
      console.log(`GET "getMACD" REQUEST`);
      _this.sendMACD(req, res);
    });
    this.app.get('/api/getOB', (req, res) => {
      console.log(`GET "getOB" REQUEST`);
    _this.sendOB(req, res);
    });
    this.app.get('/api/getStatus', (req, res) => {
      console.log(`GET "getStatus" REQUEST`);
      _this.sendStatus(req, res);
    });
  }

  init() {
    this.__ConfigExpress();
    this.__InitRoutes();

    let port = process.env.PORT || 3000;
    this.app.listen(port, () => {
      console.log(`:RequestHandler#init -> Listening on port ${port}...`);
    });
  }

  sendMACD(_Req, _Res) {
    throw new Error('Implement the method you fuck.');
  }

  sendOB(_Req, _Res) {
    throw new Error('Implement the method you fuck.');
  }

  sendStatus(_Req, _Res) {
    throw new Error('Implement the method you fuck.');
  }
}

module.exports = RequestHandler;