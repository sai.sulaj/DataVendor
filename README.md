# Data Vendor

Data vendor is an application that aggregates and parses exchange data using CCXT, and provides an API to access the data in a strategy's desired format so parsing data and abiding by ratelimits is not required by the strategies.