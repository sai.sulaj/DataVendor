/**
 * Author: Saimir Sulaj.
 * Date: May 20, 2018.
 * Purpose: Data vendor for trading bots. Built for CogniSynth Capital.
 */

// ------------- DEPENDENCIES ------------- //

const CCXT           = require('ccxt');

// ------------- LOCAL DEPENDENCIES ------------- //

const RequestHandler = require("./javascript/RequestHandler.js");
const DataFetcher    = require("./javascript/DataFetcher.js");

var   dataFetcher    = new DataFetcher();
var   requestHandler = new RequestHandler();

(async () => {
  await dataFetcher.init();
  requestHandler.init();
  implementRequestHandlerMethods(requestHandler);
  
  await dataFetcher.runLoop(() => {
    console.log('downloaded.');
  });
})();

function implementRequestHandlerMethods(_RequestHandler) {
  _RequestHandler.sendStatus = function(_Req, _Res) {
    _Res.send(dataFetcher.getStatus());
  }

  _RequestHandler.sendMACD = function(_Req, _Res) {
    if (_Req.query.period &&
        _Req.query.fast_period &&
        _Req.query.slow_period &&
        _Req.query.signal_period &&
        _Req.query.side &&
        _Req.query.exchange &&
        _Req.query.symbol) {
      
      let exchange = dataFetcher.getExchange(_Req.query.exchange.toLowerCase().replace('_', '.'));
      if (exchange === null) {
        _Res.status(400).send('Exchange not in cache');
      } else {

        let options = {
          period: parseInt(_Req.query.period),
          slow_period: parseInt(_Req.query.slow_period),
          fast_period: parseInt(_Req.query.fast_period),
          signal_period: parseInt(_Req.query.signal_period),
          side: _Req.query.side
        }
        let respData = exchange.getOBMACD(_Req.query.symbol.replace('_', '/'), options);
        _Res.send(respData);
      }
    } else {
      console.log(_Req);
      _Res.status(400).send('Invalid query parameters');
    }
  }

  _RequestHandler.sendOB = function(_Req, _Res) {
    if (_Req.query.exchange &&
        _Req.query.symbol &&
        _Req.query.depth) {
      
      let exchange = dataFetcher.getExchange(_Req.query.exchange.toLowerCase().replace('_', '.'));
      if (exchange === null) {
        _Res.status(400).send('Exchange not in cache');
      } else {
        try {
          let depth = parseInt(_Req.query.depth);
          let symbol = _Req.query.symbol.replace('_', '/');
          let numEntries = exchange.orderbookCache.getNumEntries(symbol);
          if (numEntries !== null) {
            let num = depth > numEntries ? numEntries : depth;
            let respData = exchange.orderbookCache.getRecent(symbol, num);
            _Res.send(respData);
          } else {
            _Res.status(400).send('Symbol not in cache');
          }
        } catch(err) {
          console.log(err.stack);
          _Res.status(400).send('Depth must be of type int.');
        }
      }
    } else {
      console.log(_Req);
      _Res.status(400).send('Invalid query parameters');
    }
  }
}